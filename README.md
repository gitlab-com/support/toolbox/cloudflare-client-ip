# Cloudflare client IP

## Intention

This tool is created to make the task of importing Cloudflare IP ranges as [NGINX trusted proxies](https://docs.gitlab.com/omnibus/settings/nginx.html#configuring-gitlab-trusted_proxies-and-the-nginx-real_ip-module)
just a tiny bit easier.

## Installation

This script has one external requirement: [python-cloudflare](https://github.com/cloudflare/python-cloudflare)\
Per the the maintainers _strong_ suggestion, the package is pinned to 2.19.x,
although the script itself is compatible with 3.0.0.

To install the dependencies, run `python3 -m pip install -r requirements.txt`, or
just `pip install -r requirements.txt` if you run this in a [venv](https://docs.python.org/3/library/venv.html).

## Usage

### Python script

- Step 1: Invoke the script by running `python3 cf_real_ip.py`
- Step 2: Follow the instructions printed on `STDOUT`
- ???
- Step 3: Profit!

Your GitLab instance should now populate the `remote_ip` fields with the actual client IP provided by Cloudflare.

### Docker container

TBD

### Parameters

- If you're using Pages NGINX, you might want to consider the `-p` parameter, to
also receive the configuration for it.
  - `-P` (upperscore) will _only_ print the Pages NGINX config.
- The `-r` option enables the [real_ip_recursive](https://nginx.org/en/docs/http/ngx_http_realip_module.html#real_ip_recursive) directive, setting it to "on." This configuration can be beneficial in scenarios involving multiple, chained reverse proxies.
