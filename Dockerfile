FROM python:3-alpine

RUN pip install --no-cache-dir "cloudflare==2.19.*" \
    && find /usr/lib/ -name '__pycache__' -prune -exec rm -r "{}" \;  \
    && find /usr/lib/ -name '*.pyc' -prune -exec rm -r "{}" \; 

WORKDIR /
COPY cf_real_ip.py cf_real_ip.py

ENTRYPOINT ["python", "cf_real_ip.py"]