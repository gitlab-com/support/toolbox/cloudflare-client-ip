from datetime import datetime
import argparse
import CloudFlare

# This will be the SPACER used to indent array entries:
SPACER="  "

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--pages",
                    help="Generate gitlab.rb config for Pages NGINX", action="store_true")
parser.add_argument("-P", "--pages-only",
                    help="ONLY generate gitlab.rb config for Pages NGINX", action="store_true")
parser.add_argument("-r", "--recursive",
                    help="Enables real_ip_recursive", action="store_true")
args = parser.parse_args()

def main():
    """Entry point"""
    cf = CloudFlare.CloudFlare()
    ips = cf.ips.get()
    print(f"""# Cloudflare ranges retrieved on {datetime.now()},\
for a total of {len(ips['ipv4_cidrs'])} IPv4 and {len(ips['ipv6_cidrs'])} IPv6 ranges.""")

    # Retrieve the Cloudflare IP ranges and store them in ipv4s and ipv6s lists
    ipv4s = ips["ipv4_cidrs"]
    ipv6s = ips["ipv6_cidrs"]

    if not args.pages_only:
        nginx(ipv4s, ipv6s)

    if args.pages or args.pages_only:
        # Generate gitlab.rb config for Pages NGINX
        pages_nginx(ipv4s, ipv6s)


def nginx(ipv4s, ipv6s):
    # Set Cloudflare header, see also:
    # https://developers.cloudflare.com/fundamentals/reference/http-request-headers/#cf-connecting-ip)
    print("nginx['real_ip_header'] = 'CF-Connecting-IP'")
    if args.recursive:
        # Ensure NGINX passes the header along
        print("nginx['real_ip_recursive'] = 'on'")
    # Start array for trusted addresses
    print("nginx['real_ip_trusted_addresses'] = [")

    for ip in ipv4s:
        print(f"{SPACER}\"{ip}\",")
    for ip in ipv6s:
        print(f"{SPACER}\"{ip}\",")

    # End array
    print("]\n")

def pages_nginx(ipv4s, ipv6s):
    print("pages_nginx['real_ip_header'] = 'CF-Connecting-IP'")
    if args.recursive:
        # Ensure NGINX passes the header along
        print("nginx['real_ip_recursive'] = 'on'")
    print("pages_nginx['real_ip_trusted_addresses'] = [")
    for ip in ipv4s:
        print(f"{SPACER}\"{ip}\",")
    for ip in ipv6s:
        print(f"{SPACER}\"{ip}\",")
    print("]\n")


if __name__ == '__main__':
    print("Add this to your /etc/gitlab/gitlab.rb file:")
    print("✂️---------------------------------------------✂️\n")
    main()
    print("✂️---------------------------------------------✂️")
    print("Then run: 'sudo gitlab-ctl reconfigure' and 'sudo gitlab-ctl restart nginx'")
