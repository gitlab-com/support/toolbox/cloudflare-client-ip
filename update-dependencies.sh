#!/bin/bash

set -eo pipefail

LATEST=""
TAG_MESSAGE="Chore:"

check_version(){
  # Get the latest 2.19 release from PyPi
  LATEST=$(curl --fail -s -H "Accept: application/json" "https://pypi.org/pypi/$1/json" | jq --sort-keys '.releases | keys | .[]' | grep "2.19" | sed 's/"//g' | sort -V | tail -1)
  echo "DEBUG: Got $LATEST for $1"
  if [ "$2" = "$LATEST" ]; 
  then 
    echo "🟢 $1 is up-to-date 😊"
  else 
    echo "🔴 Oh nooo! $1 requires an update!"
    TAG_MESSAGE="$TAG_MESSAGE
  - Bump $1 to $LATEST"
  fi

  case "$1" in
    "cloudflare")         VER_CLOUDFLARE="$LATEST" ;;
  esac
}

update_version_vars(){
  printf "Updating variables, setting:\$VER_CLOUDFLARE to %s\n" "$VER_CLOUDFLARE"
  
  curl --fail -s -X PUT --header "PRIVATE-TOKEN: $GLPAT" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/variables/VER_CLOUDFLARE" --form "value=$VER_CLOUDFLARE"
}

git_config(){
    # Replace the CI token with the group access token
    sed -i.bak "s/\/.*@/\/\/gl-pat:$GLPAT@/" .git/config

    # Bogus user info in order to make Git happy
    git config --global user.name "Update bot 🤖"
    git config --global user.email "beep.boop@users.noreply.gitlab.com"
}

echo "Last Cloudflare version used: $VER_CLOUDFLARE"
check_version cloudflare "$VER_CLOUDFLARE"

if [ "$TAG_MESSAGE" != "Chore:" ]; then
  if [ "$CI_COMMIT_BRANCH" = "main" ]; then
    # Create a new tag. Let's find the latest tag and increment by one:

    VERSION=$(git describe --abbrev=0 --tags)
    # shellcheck disable=SC2206
    VERSION_BITS=(${VERSION//./ })

    VNUM1=${VERSION_BITS[0]}
    VNUM2=${VERSION_BITS[1]}
    VNUM3=${VERSION_BITS[2]}
    VNUM3=$((VNUM3+1))

    NEW_TAG="$VNUM1.$VNUM2.$VNUM3"

    # Got it. Let's log that.
    echo "Updating $VERSION to $NEW_TAG"

    # Now configure Git and push
    git_config
    git tag -a "$NEW_TAG" -m "$TAG_MESSAGE"
    git push --tags && update_version_vars
  else
    # We're not on main, we do not want to create a tag.
    echo "🟡 Updates are required, but this isn't executed on main branch 🤷"
  fi
fi
